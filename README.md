![](https://gitlab.com/phreakuencies/prstux/raw/master/boot/splash/logo.png)

**PRSTUX** is a Linux (Debian) based distribution for the Sony PRS-T series of readers. It is built around KOreader as the main application. For complete documentation, please visit the [Wiki](https://gitlab.com/phreakuencies/prstux/wikis/home).
